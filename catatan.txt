// Membuat database
CREATE DATABASE databasename;

// Mengaktifkan database
USE databasename;

// Menampilkan list database
SHOW DATABASES;

// menghapus database 
DROP DATABASE databasename;

// Membuat tabel
CREATE TABLE tablename (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(255),
  email varchar(255),
  password varchar(255)
);

// Menampilkan list tabel
SHOW TABLES;

// Menampilkan struktur tabel
DESCRIBE tablename;


// Menghubungkan tabel items dengan tabel categories
ALTER TABLE tablename
ADD FOREIGN KEY (column) REFERENCES targetTable(targetColumn);

// menghapus table 
DROP TABLE tablename;


// Menambahkan data ke tabel
INSERT INTO tablename VALUES 
  ('', 'John Doe', 'john@doe.com', 'john123'),
  ('', 'Jane Doe', 'jane@doe.com', 'jane123');

// Melihat semua data dari tabel
SELECT * 
FROM tablename;

// Melihat kolom data dari tabel
SELECT name, email 
FROM tablename;

// Melihat baris spesifik dari tabel
SELECT * 
FROM tablename 
WHERE columnname = search;

// Melihat baris spesifik dari tabel
SELECT * 
FROM tablename 
WHERE columnname LIKE '%search%';

// Mengubah data
UPDATE tablename 
SET columnname = newvalue
WHERE columnname = search;

// Menghapus data 
DELETE FROM tablename 
WHERE columnname = search;

